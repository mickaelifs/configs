#!/bin/sh

#install openvn
sudo apt install openvpn ranger highlight

#setup zap.sh
mkdir -p ~/.bin
cp zap.sh ~/.bin

#setup git prompt
git clone https://github.com/magicmonty/bash-git-prompt.git ~/.bash-git-prompt --depth=1

#setup vim plugins and environnement
cp .vimrc ~

#Install pathogen plugin manager
sudo apt install curl
mkdir -p ~/.vim/autoload ~/.vim/bundle && \
curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim

#Vim airline
git clone https://github.com/vim-airline/vim-airline ~/.vim/bundle/vim-airline
#Vim ansible-yaml
git clone https://github.com/chase/vim-ansible-yaml.git ~/.vim/bundle/vim-ansible-yaml
#Vim jinja
git clone https://github.com/lepture/vim-jinja.git ~/.vim/bundle/vim-jinja
#Vim tabline
git clone https://github.com/mkitt/tabline.vim.git ~/.vim/bundle/tabline.vim
#Vim Hexmode
git clone https://github.com/fidian/hexmode.git  ~/.vim/bundle/hexmode

#Vim Vundle plugin mananger
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

#Vim YouCompleteMe
cp .ycm_extra_conf.py ~/.vim
git clone https://github.com/ycm-core/YouCompleteMe.git ~/.vim/bundle/YouCompleteMe
sudo apt install build-essential cmake vim python3-dev golang
cd ~/.vim/bundle/YouCompleteMe && git submodule update --init --recursive && python3 install.py --all
