"Hardtime :no hjkl or arrow repeat to learn vim
"let g:hardtime_default_on = 1
colorscheme slate
au BufWinLeave * silent! mkview
au BufWinEnter * silent! loadview
set hlsearch
set nocompatible
set listchars=eol:¬,tab:>·,trail:~,extends:>,precedes:<,space:␣
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
let g:ycm_global_ycm_extra_conf = "~/.vim/.ycm_extra_conf.py"
let g:ycm_confirm_extra_conf=0
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
"Plugin 'Valloric/YouCompleteMe'
call vundle#end()
execute pathogen#infect()
syntax on
filetype plugin indent on
set laststatus=2
let mapleader=" "
set nu
set ts=4
set shiftwidth=4
set expandtab
"line wrap to 80 char
"set textwidth=80
"set wrap

nnoremap <Leader>w :w<CR>
nnoremap <Leader>o :CtrlP<CR>

" Tags
nmap <leader>c :tab split<CR>:exec("tag ".expand("<cword>"))<CR>
" To open a new empty buffer
" This replaces :tabnew which I used to bind to this mapping
nmap <leader>t :tabnew<cr>

" Move to the next tab
nmap <leader>l :tabn<CR>

" Move to the previous buffer
nmap <leader>h :tabp<CR>

" Close the current buffer and move to the previous one
" This replicates the idea of closing a tab
nmap <leader>bq :bp <BAR> bd #<CR>

" Show all open buffers and their status
nmap <leader>bl :ls<CR>

" Java complete (Ctrl+X, Ctrl+O)
setlocal omnifunc=javacomplete#Complete

com! FormatJSON %!python -m json.tool
noremap Q !!sh<CR>
nmap  ;l   :call ListTrans_toggle_format()<CR>
xmap <silent><expr>  ++  VMATH_YankAndAnalyse()
